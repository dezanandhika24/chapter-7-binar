var express = require('express');
var router = express.Router();
const Article = require('../models/Article')

/* GET home page. */
router.get('/', function(req, res, next) {
  res.render('index', { title: 'Express' });
});

router.get('/about', function(req, res, next) {
  res.render('about', { title: 'Express' });
})

router.get('/articles', (req, res) => {
  Article.findAll().then(articles => {
    res.render('articles', {articles})
  })
});

module.exports = router;
